//=============================================================================================
// A beadott program csak ebben a fajlban lehet, a fajl 1 byte-os ASCII karaktereket tartalmazhat, BOM kihuzando.
// Tilos:
// - mast "beincludolni", illetve mas konyvtarat hasznalni
// - faljmuveleteket vegezni a printf-et kiveve
// - Mashonnan atvett programresszleteket forrasmegjeloles nelkul felhasznalni es
// - felesleges programsorokat a beadott programban hagyni!!!!!!!
// - felesleges kommenteket a beadott programba irni a forrasmegjelolest kommentjeit kiveve
// ---------------------------------------------------------------------------------------------
// A feladatot ANSI C++ nyelvu forditoprogrammal ellenorizzuk, a Visual Studio-hoz kepesti elteresekrol
// es a leggyakoribb hibakrol (pl. ideiglenes objektumot nem lehet referencia tipusnak ertekul adni)
// a hazibeado portal ad egy osszefoglalot.
// ---------------------------------------------------------------------------------------------
// A feladatmegoldasokban csak olyan OpenGL fuggvenyek hasznalhatok, amelyek az oran a feladatkiadasig elhangzottak
// A keretben nem szereplo GLUT fuggvenyek tiltottak.
//
// NYILATKOZAT
// ---------------------------------------------------------------------------------------------
// Nev    : Szommer Zsombor
// Neptun : MM5NOT
// ---------------------------------------------------------------------------------------------
// ezennel kijelentem, hogy a feladatot magam keszitettem, es ha barmilyen segitseget igenybe vettem vagy
// mas szellemi termeket felhasznaltam, akkor a forrast es az atvett reszt kommentekben egyertelmuen jeloltem.
// A forrasmegjeloles kotelme vonatkozik az eloadas foliakat es a targy oktatoi, illetve a
// grafhazi doktor tanacsait kiveve barmilyen csatornan (szoban, irasban, Interneten, stb.) erkezo minden egyeb
// informaciora (keplet, program, algoritmus, stb.). Kijelentem, hogy a forrasmegjelolessel atvett reszeket is ertem,
// azok helyessegere matematikai bizonyitast tudok adni. Tisztaban vagyok azzal, hogy az atvett reszek nem szamitanak
// a sajat kontribucioba, igy a feladat elfogadasarol a tobbi resz mennyisege es minosege alapjan szuletik dontes.
// Tudomasul veszem, hogy a forrasmegjeloles kotelmenek megsertese eseten a hazifeladatra adhato pontokat
// negativ elojellel szamoljak el es ezzel parhuzamosan eljaras is indul velem szemben.
//=============================================================================================

#include "framework.h"

inline vec3 &operator+=(vec3 &v1, const vec3 &v2)
{
	return v1 = v1 + v2;
}

inline vec3 &operator-=(vec3 &v1, const vec3 &v2)
{
	return v1 = v1 - v2;
}

inline vec4 &operator-=(vec4 &v1, const vec4 &v2)
{
	return v1 = v1 - v2;
}

float randomFloat(float from, float to)
{
	return from + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / (to - from)));
}

template <class T>
struct Dnum
{
	float f;
	T d;

	Dnum(float f0 = 0, T d0 = T(0))
	{
		this->f = f0;
		this->d = d0;
	}

	Dnum operator+(Dnum r)
	{
		return Dnum(f + r.f, d + r.d);
	}

	Dnum operator-(Dnum r)
	{
		return Dnum(f - r.f, d - r.d);
	}

	Dnum operator*(Dnum r)
	{
		return Dnum(f * r.f, f * r.d + d * r.f);
	}

	Dnum operator/(Dnum r)
	{
		return Dnum(f / r.f, (r.f * d - r.d * f) / r.f / r.f);
	}
};

template <class T>
Dnum<T> Exp(Dnum<T> g) { return Dnum<T>(expf(g.f), expf(g.f) * g.d); }
template <class T>
Dnum<T> Sin(Dnum<T> g) { return Dnum<T>(sinf(g.f), cosf(g.f) * g.d); }
template <class T>
Dnum<T> Cos(Dnum<T> g) { return Dnum<T>(cosf(g.f), -sinf(g.f) * g.d); }
template <class T>
Dnum<T> Tan(Dnum<T> g) { return Sin(g) / Cos(g); }
template <class T>
Dnum<T> Sinh(Dnum<T> g) { return Dnum<T>(sinhf(g.f), coshf(g.f) * g.d); }
template <class T>
Dnum<T> Cosh(Dnum<T> g) { return Dnum<T>(coshf(g.f), sinhf(g.f) * g.d); }
template <class T>
Dnum<T> Tanh(Dnum<T> g) { return Sinh(g) / Cosh(g); }
template <class T>
Dnum<T> Log(Dnum<T> g) { return Dnum<T>(logf(g.f), g.d / g.f); }
template <class T>
Dnum<T> Pow(Dnum<T> g, float n) { return Dnum<T>(powf(g.f, n), n * powf(g.f, n - 1) * g.d); }

typedef Dnum<vec2> Dnum2;

const int tessellationLevel = 100;

struct Material
{
	vec3 kd, ks, ka;
	float shininess;
};

struct RandomMaterial : public Material
{
	RandomMaterial()
	{
		float specular = randomFloat(0, 5);
		this->kd = vec3(randomFloat(0, 1), randomFloat(0, 1), randomFloat(0, 1));
		this->ks = vec3(specular, specular, specular);
		this->ka = this->kd * (float)M_PI;
		this->shininess = randomFloat(1, 100);
	}
};

inline vec4 qnorm(vec4 q)
{
	vec3 d = normalize(vec3(q.x, q.y, q.z)) * sqrtf(1 - q.w * q.w);
	return vec4(d.x, d.y, d.z, q.w);
}

vec4 qmul(vec4 q1, vec4 q2)
{
	vec3 d1 = vec3(q1.x, q1.y, q1.z), d2 = vec3(q2.x, q2.y, q2.z);
	vec3 v = vec3(d2 * q1.w + d1 * q2.w + cross(d1, d2));
	return vec4(v.x, v.y, v.z, q1.w * q2.w - dot(d1, d2));
}

inline vec3 rotate(vec3 u, vec4 q)
{
	vec4 qinv = vec4(-q.x, -q.y, -q.z, q.w);
	vec4 qr = qmul(qmul(q, vec4(u.x, u.y, u.z, 0)), qinv);
	return vec3(qr.x, qr.y, qr.z);
}

struct Light
{
	vec3 La, Le;
	vec3 wLightPos;
	vec3 start;
	vec3 origo;

	void Animate(const float tstart, const float tend)
	{
		float t = tend;
		vec4 q = vec4(cosf(t / 4), sinf(t / 4) * cosf(t) / 2, sinf(t / 4) * sinf(t) / 2, sinf(t / 4) * sqrtf(3.0f / 4.0f));
		q = qnorm(q);
		this->wLightPos = rotate(this->start - this->origo, q) + this->origo;
	}
};

class RandomColorTexture : public Texture
{
public:
	RandomColorTexture() : Texture()
	{
		std::vector<vec4> image(1);
		float red = (float)(rand() % 256) / 256;
		float green = (float)(rand() % 256) / 256;
		float blue = (float)(rand() % 256) / 256;
		image[0] = vec4(red, green, blue, 1.0f);
		this->create(1, 1, image, GL_NEAREST);
	}
};

struct RenderState
{
	mat4 MVP, M, Minv, V, P;
	Material *material;
	std::vector<Light> lights;
	Texture *texture;
	vec3 wEye;
};

class Shader : public GPUProgram
{
public:
	virtual void Bind(RenderState state) = 0;

	void setUniformMaterial(const Material &material, const std::string &name)
	{
		this->setUniform(material.kd, name + ".kd");
		this->setUniform(material.ks, name + ".ks");
		this->setUniform(material.ka, name + ".ka");
		this->setUniform(material.shininess, name + ".shininess");
	}

	void setUniformLight(const Light &light, const std::string &name)
	{
		this->setUniform(light.La, name + ".La");
		this->setUniform(light.Le, name + ".Le");
		this->setUniform(light.wLightPos, name + ".wLightPos");
	}
};

class PhongShader : public Shader
{
	const char *vertexSource = R"(
		#version 330
		precision highp float;

		struct Light
		{
			vec3 La, Le;
			vec3 wLightPos;
		};

		uniform mat4  MVP, M, Minv;
		uniform Light[8] lights;
		uniform int   nLights;
		uniform vec3  wEye;

		layout(location = 0) in vec3  vtxPos;
		layout(location = 1) in vec3  vtxNorm;
		layout(location = 2) in vec2  vtxUV;

		out vec3 wNormal;
		out vec3 wView;
		out vec3 wLight[8];
		out vec2 texcoord;

		void main()
		{
			gl_Position = vec4(vtxPos, 1) * MVP;

			vec4 wPos = vec4(vtxPos, 1) * M;
			for(int i = 0; i < nLights; i++)
			{
				wLight[i] = lights[i].wLightPos * wPos.w - wPos.xyz;
			}
		    wView  = wEye * wPos.w - wPos.xyz;
		    wNormal = (Minv * vec4(vtxNorm, 0)).xyz;
		    texcoord = vtxUV;
		}
	)";

	const char *fragmentSource = R"(
		#version 330
		precision highp float;

		struct Light
		{
			vec3 La, Le;
			vec3 wLightPos;
		};

		struct Material
		{
			vec3 kd, ks, ka;
			float shininess;
		};

		uniform Material material;
		uniform Light[8] lights;
		uniform int   nLights;
		uniform sampler2D diffuseTexture;

		in  vec3 wNormal;
		in  vec3 wView;
		in  vec3 wLight[8];
		in  vec2 texcoord;
		
        out vec4 fragmentColor;

		void main()
		{
			vec3 N = normalize(wNormal);
			vec3 V = normalize(wView); 
			if (dot(N, V) < 0) N = -N;
			vec3 texColor = texture(diffuseTexture, texcoord).rgb;
			vec3 ka = material.ka * texColor;
			vec3 kd = material.kd * texColor;

			vec3 radiance = vec3(0, 0, 0);
			for(int i = 0; i < nLights; i++)
			{
				vec3 L = normalize(wLight[i]);
				vec3 H = normalize(L + V);
				float cost = max(dot(N,L), 0), cosd = max(dot(N,H), 0);
				radiance += ka * lights[i].La + (kd * texColor * cost + material.ks * pow(cosd, material.shininess)) * lights[i].Le;
			}
			fragmentColor = vec4(radiance, 1);
		}
	)";

public:
	PhongShader()
	{
		this->create(vertexSource, fragmentSource, "fragmentColor");
	}

	void Bind(RenderState state)
	{
		this->Use();
		this->setUniform(state.MVP, "MVP");
		this->setUniform(state.M, "M");
		this->setUniform(state.Minv, "Minv");
		this->setUniform(state.wEye, "wEye");
		this->setUniform(*state.texture, std::string("diffuseTexture"));
		this->setUniformMaterial(*state.material, "material");
		this->setUniform((int)state.lights.size(), "nLights");
		for (unsigned int i = 0; i < state.lights.size(); ++i)
		{
			this->setUniformLight(state.lights[i], std::string("lights[") + std::to_string(i) + std::string("]"));
		}
	}
};

class Geometry
{
protected:
	unsigned int vao, vbo;

public:
	Geometry()
	{
		glGenVertexArrays(1, &this->vao);
		glBindVertexArray(this->vao);
		glGenBuffers(1, &this->vbo);
		glBindBuffer(GL_ARRAY_BUFFER, this->vbo);
	}

	virtual void Draw() = 0;

	~Geometry()
	{
		glDeleteBuffers(1, &this->vbo);
		glDeleteVertexArrays(1, &this->vao);
	}
};

class ParamSurface : public Geometry
{
	unsigned int nVtxPerStrip, nStrips;

public:
	ParamSurface()
	{
		this->nVtxPerStrip = this->nStrips = 0;
	}

	virtual void eval(Dnum2 &U, Dnum2 &V, Dnum2 &X, Dnum2 &Y, Dnum2 &Z) = 0;

	struct VertexData
	{
		vec3 position, normal;
		vec2 texcoord;
	};

	VertexData GenVertexData(float u, float v)
	{
		VertexData vtxData;
		vtxData.texcoord = vec2(u, v);
		Dnum2 X, Y, Z;
		Dnum2 U(u, vec2(1, 0)), V(v, vec2(0, 1));
		this->eval(U, V, X, Y, Z);
		vtxData.position = vec3(X.f, Y.f, Z.f);
		vec3 drdU(X.d.x, Y.d.x, Z.d.x), drdV(X.d.y, Y.d.y, Z.d.y);
		vtxData.normal = cross(drdU, drdV);
		return vtxData;
	}

	void create(const int N = tessellationLevel, const int M = tessellationLevel)
	{
		glBindVertexArray(this->vao);
		glBindBuffer(GL_ARRAY_BUFFER, this->vbo);

		this->nVtxPerStrip = (M + 1) * 2;
		this->nStrips = N;
		std::vector<VertexData> vtxData;
		for (int i = 0; i < N; ++i)
		{
			for (int j = 0; j <= M; ++j)
			{
				vtxData.push_back(GenVertexData((float)j / M, (float)i / N));
				vtxData.push_back(GenVertexData((float)j / M, (float)(i + 1) / N));
			}
		}
		glBufferData(GL_ARRAY_BUFFER, this->nVtxPerStrip * this->nStrips * sizeof(VertexData), &vtxData[0], GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (void *)offsetof(VertexData, position));
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (void *)offsetof(VertexData, normal));
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(VertexData), (void *)offsetof(VertexData, texcoord));
	}

	void Draw()
	{
		glBindVertexArray(this->vao);
		for (unsigned int i = 0; i < this->nStrips; ++i)
		{
			glDrawArrays(GL_TRIANGLE_STRIP, i * this->nVtxPerStrip, this->nVtxPerStrip);
		}
	}
};

class Sphere : public ParamSurface
{
public:
	Sphere(int sphereTessellationLevel = tessellationLevel)
	{
		this->create(sphereTessellationLevel, sphereTessellationLevel);
	}

	void eval(Dnum2 &U, Dnum2 &V, Dnum2 &X, Dnum2 &Y, Dnum2 &Z)
	{
		U = U * 2.0f * (float)M_PI;
		V = V * (float)M_PI;
		X = Cos(U) * Sin(V);
		Z = Sin(U) * Sin(V);
		Y = Cos(V);
	}
};

struct BlackHole
{
	float x, z;
	float mass;

	BlackHole(vec2 pos, float mass)
	{
		this->x = pos.x;
		this->z = pos.y;
		this->mass = mass;
	}
};

class RubberSheet : public ParamSurface
{
private:
	float nextMass;
	std::vector<BlackHole> blackHoles;

public:
	RubberSheet(int rubberSheetTessellationLevel = tessellationLevel)
	{
		this->create(rubberSheetTessellationLevel, rubberSheetTessellationLevel);
		this->nextMass = 0;
	}

	void eval(Dnum2 &U, Dnum2 &V, Dnum2 &X, Dnum2 &Y, Dnum2 &Z)
	{
		U = U * 2.0f - 1.0f;
		V = V * 2.0f - 1.0f;
		X = U;
		Z = V;
		Y = 0;
		for (auto blackHole : this->blackHoles)
		{
			Y = Y - Dnum2(blackHole.mass, 0) * Pow(Pow(Pow(X - blackHole.x, 2) + Pow(Z - blackHole.z, 2), 0.5f) + Dnum2(0.01f, 0), -1.0f);
		}
	}

	void addBlackHole(vec2 blackHolePos)
	{
		BlackHole blackHole = BlackHole(blackHolePos, this->nextMass += 0.01f);
		if (-1 < blackHole.x && blackHole.x < 1 && -1 < blackHole.z && blackHole.z < 1)
		{
			this->blackHoles.push_back(blackHole);
		}
		this->create();
	}

	void clearBlackHoles()
	{
		this->nextMass = 0;
		this->blackHoles.clear();
		this->create();
	}

	Texture *generateTexture(const int N = windowWidth, const int M = windowHeight)
	{

		std::vector<vec4> image(N * M);
		const vec4 yellow(1, 1, 0, 1), blue(0, 0, 1, 1);
		for (int x = 0; x < N; ++x)
		{
			for (int y = 0; y < M; ++y)
			{
				ParamSurface::VertexData vertexData = this->GenVertexData((float)x / N, (float)y / M);
				float height = vertexData.position.y - 0.01f;
				image[y * N + x] = vec4(floorf((1 + 4 * height) * 8) / 8, floorf((1 + 4 * height) * 8) / 8, floorf((1 + 4 * height) * 8) / 8, 1);
			}
		}
		return new Texture(N, M, image);
	}
};

struct Object
{
	Shader *shader;
	Material *material;
	Texture *texture;
	Geometry *geometry;
	vec3 scale, translation, rotationAxis;
	float rotationAngle;

public:
	Object(Shader *_shader, Material *_material, Texture *_texture, Geometry *_geometry) : scale(1, 1, 1), translation(0, 0, 0), rotationAxis(0, 0, 1), rotationAngle(0)
	{
		this->shader = _shader;
		this->material = _material;
		this->texture = _texture;
		this->geometry = _geometry;
	}

	virtual void SetModelingTransform(mat4 &M, mat4 &Minv)
	{
		M = ScaleMatrix(this->scale) * RotationMatrix(this->rotationAngle, this->rotationAxis) * TranslateMatrix(this->translation);
		Minv = TranslateMatrix(-this->translation) * RotationMatrix(-this->rotationAngle, this->rotationAxis) * ScaleMatrix(vec3(1 / this->scale.x, 1 / this->scale.y, 1 / this->scale.z));
	}

	virtual void Draw(RenderState state)
	{
		mat4 M, Minv;
		this->SetModelingTransform(M, Minv);
		state.M = M;
		state.Minv = Minv;
		state.MVP = state.M * state.V * state.P;
		state.material = this->material;
		state.texture = this->texture;
		this->shader->Bind(state);
		this->geometry->Draw();
	}

	virtual void Animate(const float tstart, const float tend)
	{
	}

	virtual Object *clone()
	{
		Object *clone = new Object(this->shader, this->material, this->texture, this->geometry);
		clone->scale = this->scale;
		clone->translation = this->translation;
		clone->rotationAxis = this->rotationAxis;
		clone->rotationAngle = this->rotationAngle;
		return clone;
	}

	virtual ~Object()
	{
		delete this->material;
		delete this->texture;
	}
};

vec3 g = vec3(0, -9.81f, 0);

struct MovingObject : Object
{
	static Object *rubberSheetObject;

	float m;
	int launched;
	vec3 normal;
	vec3 acceleration;
	vec3 velocity;
	float initialEnergy;

	MovingObject(Shader *_shader, Material *_material, Texture *_texture, Geometry *_geometry) : Object(_shader, _material, _texture, _geometry), normal(0, 1, 0), acceleration(0, 0, 0), velocity(1, 0, -1)
	{
		this->m = 1.0f;
		this->launched = 0;
	}

	void Draw(RenderState state)
	{
		if (ParamSurface *paramSurface = dynamic_cast<ParamSurface *>(MovingObject::rubberSheetObject->geometry))
		{
			this->translation -= this->normal * this->scale;
			Object::Draw(state);
			this->translation += this->normal * this->scale;
		}
	}

	void launch(vec2 force)
	{
		this->launched = 1;
		this->velocity = vec3(force.x, 0, force.y);
		this->initialEnergy = 1.0f / 2.0f * this->m * dot(this->velocity, this->velocity) + this->m * length(g) * this->translation.y;
	}

	MovingObject *clone()
	{
		MovingObject *clone = new MovingObject(this->shader, this->material, this->texture, this->geometry);
		clone->scale = this->scale;
		clone->translation = this->translation;
		clone->rotationAxis = this->rotationAxis;
		clone->rotationAngle = this->rotationAngle;
		clone->velocity = this->velocity;
		clone->launched = this->launched;
		return clone;
	}

	void move(float dt)
	{
		this->acceleration = g - this->normal * dot(g, this->normal);
		this->velocity += this->acceleration * dt;
		this->translation += this->velocity * dt;
	}

	void torusTopology()
	{
		if (this->translation.x < -1)
		{
			this->translation.x += 2;
		}
		if (1 < this->translation.x)
		{
			this->translation.x -= 2;
		}
		if (this->translation.z < -1)
		{
			this->translation.z += 2;
		}
		if (1 < this->translation.z)
		{
			this->translation.z -= 2;
		}
	}

	void corrigate(float height)
	{
		this->translation.y = height;
		float kineticEnergy = this->initialEnergy - this->m * length(g) * this->translation.y;
		float speed = sqrtf(2.0f * kineticEnergy / this->m);
		this->velocity = normalize(this->velocity);
		this->velocity = this->velocity * speed;
	}

	void Animate(float tstart, float tend)
	{
		if (ParamSurface *paramSurface = dynamic_cast<ParamSurface *>(MovingObject::rubberSheetObject->geometry))
		{
			float u = (this->translation.x + 1.0f) / 2.0f;
			float v = (this->translation.z + 1.0f) / 2.0f;
			ParamSurface::VertexData vertexData = paramSurface->GenVertexData(u, v);
			this->normal = normalize(vertexData.normal);
			if (!this->launched)
			{
				return;
			}
			this->move(tend - tstart);
			this->torusTopology();
			vertexData = paramSurface->GenVertexData(u, v);
			this->corrigate(vertexData.position.y);
		}
	}

	~MovingObject()
	{
	}
};

Object *MovingObject::rubberSheetObject = NULL;

struct Camera
{
	vec3 wEye, wLookat, wVup;
	float fov, asp, fp, bp;
	MovingObject *objectToFollow;

public:
	Camera()
	{
		this->asp = (float)windowWidth / windowHeight;
		this->fov = 90.0f * (float)M_PI / 180.0f;
		this->fp = 0.01f;
		this->bp = 100;
		this->objectToFollow = NULL;
	}

	mat4 V()
	{
		vec3 w = normalize(this->wEye - this->wLookat);
		vec3 u = normalize(cross(this->wVup, w));
		vec3 v = cross(w, u);
		return TranslateMatrix(this->wEye * (-1)) * mat4(u.x, v.x, w.x, 0,
														 u.y, v.y, w.y, 0,
														 u.z, v.z, w.z, 0,
														 0, 0, 0, 1);
	}

	mat4 P()
	{
		return mat4(1 / (tan(this->fov / 2) * this->asp), 0, 0, 0,
					0, 1 / tan(this->fov / 2), 0, 0,
					0, 0, -(this->fp + this->bp) / (this->bp - this->fp), -1,
					0, 0, -2 * this->fp * this->bp / (this->bp - this->fp), 0);
	}

	void Animate(float tstart, float tend)
	{
		if (this->objectToFollow == NULL)
		{
			this->wEye = vec3(0, 1, 0);
			this->wLookat = vec3(0, 0, 0);
			this->wVup = vec3(0, 0, -1);
		}
		else
		{
			this->wEye = this->objectToFollow->translation - this->objectToFollow->normal * this->objectToFollow->scale;
			this->wLookat = this->objectToFollow->translation + this->objectToFollow->velocity;
			this->wVup = -this->objectToFollow->normal;
		}
	}
};

class Scene
{
	std::vector<MovingObject *> objects;
	Camera camera;
	std::vector<Light> lights;
	int following = 0;

	void removeObject(MovingObject *object)
	{
		for (int i = this->objects.size() - 1; i >= 0; --i)
		{
			if (this->objects[i] == object)
			{
				delete this->objects[i];
				this->objects.erase(this->objects.begin() + i);
				break;
			}
		}
	}

	Shader *phongShader;
	RubberSheet *rubberSheet;
	Geometry *sphere;

public:
	Object *rubberSheetObject;

	void Build()
	{
		this->lights.resize(2);
		this->lights[0].wLightPos = vec3(-2, 2, 0);
		this->lights[0].start = this->lights[0].wLightPos;
		this->lights[0].La = vec3(0.5f, 0.5f, 0.5f);
		this->lights[0].Le = vec3(0.3f, 0.1f, 0.1f);
		this->lights[1].wLightPos = vec3(2, 2, 0);
		this->lights[1].start = this->lights[1].wLightPos;
		this->lights[1].La = vec3(0.5f, 0.5f, 0.5f);
		this->lights[1].Le = vec3(0.1f, 0.3f, 0.3f);
		this->lights[0].origo = this->lights[1].wLightPos;
		this->lights[1].origo = this->lights[0].wLightPos;

		this->phongShader = new PhongShader();

		Material *material = new Material();
		material->kd = vec3(0.2f, 0.2f, 0.2f);
		material->ks = vec3(3, 3, 3);
		material->ka = material->kd * (float)M_PI;
		material->shininess = 30;

		this->rubberSheet = new RubberSheet();
		this->rubberSheetObject = new Object(phongShader, material, rubberSheet->generateTexture(), rubberSheet);
		MovingObject::rubberSheetObject = this->rubberSheetObject;

		this->sphere = new Sphere();
		MovingObject *sphereObject = new MovingObject(phongShader, new RandomMaterial(), new RandomColorTexture(), sphere);
		sphereObject->translation = vec3(-0.9f, 0, 0.9f);
		sphereObject->scale = vec3(0.03f, 0.03f, 0.03f);
		this->objects.push_back(sphereObject);
	}

	void launchSphere(vec2 force)
	{
		MovingObject *last = this->objects[this->objects.size() - 1];
		MovingObject *clone = last->clone();
		clone->material = new RandomMaterial();
		clone->texture = new RandomColorTexture();
		this->objects.push_back(clone);
		last->launch(force / 2);
	}

	void addBlackHole(vec2 pos)
	{
		if (RubberSheet *rubberSheet = dynamic_cast<RubberSheet *>(this->rubberSheetObject->geometry))
		{
			rubberSheet->addBlackHole(pos);
			delete rubberSheetObject->texture;
			rubberSheetObject->texture = rubberSheet->generateTexture();
		}
	}

	void clearBlackHoles()
	{
		if (RubberSheet *rubberSheet = dynamic_cast<RubberSheet *>(this->rubberSheetObject->geometry))
		{
			rubberSheet->clearBlackHoles();
			delete rubberSheetObject->texture;
			rubberSheetObject->texture = rubberSheet->generateTexture();
		}
	}

	void Render()
	{
		RenderState state;
		state.wEye = camera.wEye;
		state.V = camera.V();
		state.P = camera.P();
		state.lights = lights;
		this->rubberSheetObject->Draw(state);
		for (auto obj : this->objects)
		{
			if (this->camera.objectToFollow == obj)
			{
				continue;
			}
			obj->Draw(state);
		}
	}

	void Animate(float tstart, float tend)
	{
		for (int i = 0; i < this->lights.size(); ++i)
		{
			lights[i].Animate(tstart, tend);
		}
		for (auto obj : this->objects)
		{
			obj->Animate(tstart, tend);
			if (obj->translation.y < -0.99f)
			{
				if (this->camera.objectToFollow == obj)
				{
					this->camera.objectToFollow = NULL;
				}
				this->removeObject(obj);
			}
		}
		if (this->following)
		{
			this->camera.objectToFollow = this->objects[0];
		}
		this->camera.Animate(tstart, tend);
	}

	void attachCamera()
	{
		if (this->following)
		{
			this->following = 0;
			this->camera.objectToFollow = NULL;
		}
		else
		{
			this->following = 1;
		}
	}

	~Scene()
	{
		delete this->rubberSheetObject;
		for (auto obj : this->objects)
		{

			delete obj;
		}
		delete this->phongShader;
		delete this->rubberSheet;
		delete this->sphere;
	}
};

Scene scene;

void onInitialization()
{
	glViewport(0, 0, windowWidth, windowHeight);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	scene.Build();
}

void onDisplay()
{
	glClearColor(0.5f, 0.5f, 0.8f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	scene.Render();
	glutSwapBuffers();
}

void onKeyboard(unsigned char key, int pX, int pY)
{
	switch (key)
	{
	case ' ':
		scene.attachCamera();
		break;
	case 'd':
		scene.clearBlackHoles();
		break;
	}
}

void onKeyboardUp(unsigned char key, int pX, int pY)
{
}

void onMouse(int button, int state, int pX, int pY)
{
	float cX = 2.0f * pX / windowWidth - 1;
	float cY = 2.0f * pY / windowWidth - 1;

	if (state == GLUT_DOWN)
	{
		switch (button)
		{
		case GLUT_LEFT_BUTTON:
			scene.launchSphere(vec2(cX + 1, cY - 1));
			break;
		case GLUT_RIGHT_BUTTON:
			scene.addBlackHole(vec2(cX, cY));
			break;
		}
	}
}

void onMouseMotion(int pX, int pY)
{
}

void onIdle()
{
	static float tend = 0;
	const float dt = 0.01f;
	float tstart = tend;
	tend = glutGet(GLUT_ELAPSED_TIME) / 1000.0f;

	for (float t = tstart; t < tend; t += dt)
	{
		float Dt = fmin(dt, tend - t);
		scene.Animate(t, t + Dt);
	}
	glutPostRedisplay();
}
